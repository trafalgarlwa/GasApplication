package com.example.gasapplication.ui.binding

data class BindingState(
    val success: Int? = null,
    val notFoundErr: Int? = null,
    val networkErr: Int? = null
)