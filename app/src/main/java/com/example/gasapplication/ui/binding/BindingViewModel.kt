package com.example.gasapplication.ui.binding

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.gasapplication.R
import com.example.gasapplication.api.ApiService
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BindingViewModel @ViewModelInject constructor(
    private val apiService: ApiService
) : ViewModel() {
    private val _bindingState = MutableLiveData<BindingState>()
    val bindingState = _bindingState

    fun binding(link: String) {
        apiService.bindingDevice(link).enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                when (response.code()) {
                    201 -> {
                        _bindingState.value = BindingState(success = R.string.binding_success)
                    }
                    404 -> {
                        _bindingState.value = BindingState(notFoundErr = R.string.device_not_found)
                    }
                    else -> {
                        _bindingState.value = BindingState(networkErr = R.string.network_exception)
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                Log.w(TAG, t)
                _bindingState.value = BindingState(networkErr = R.string.network_exception)
            }
        })
    }

    companion object {
        const val TAG = "BindingViewModel"
    }
}