package com.example.gasapplication.ui.lists

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.example.gasapplication.R

class SingleLineItem(context: Context?, attrs: AttributeSet?) : LinearLayout(context, attrs) {
    var icon: ImageView
    var text: TextView

    init {
        val root = LayoutInflater.from(context)
            .inflate(R.layout.material_list_item_single_line, this, true)
        icon = root.findViewById(R.id.mtrl_list_item_icon)
        text = root.findViewById(R.id.mtrl_list_item_text)
        context?.obtainStyledAttributes(attrs, R.styleable.SingleLineItem)?.run {
            icon.setImageResource(getResourceId(R.styleable.SingleLineItem_icon, 0))
            text.text = getString(R.styleable.SingleLineItem_text)
            recycle()
        }
    }

}