package com.example.gasapplication.ui.mine

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.components.nav.NavItem
import com.example.gasapplication.R
import com.example.gasapplication.ui.login.LoggedInUserView
import dagger.hilt.android.AndroidEntryPoint

private const val TAG = "MineFragment"

@AndroidEntryPoint
class MineFragment : Fragment() {

    private lateinit var mineViewModel: MineViewModel

    private lateinit var accountLayout: RelativeLayout
    private lateinit var loginName: TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mineViewModel =
            ViewModelProvider(this).get(MineViewModel::class.java)

        val root = inflater.inflate(R.layout.fragment_mine, container, false)
        accountLayout = root.findViewById(R.id.account_layout)
        loginName = root.findViewById(R.id.login_name)

        val navItemSettings = root.findViewById<NavItem>(R.id.navigation_settings)

        // Set up items
        navItemSettings.setOnClickListener {
            findNavController().navigate(R.id.navigation_settings)
        }

        mineViewModel.loadUser()
        mineViewModel.userState.observe(viewLifecycleOwner, Observer { userState ->
            userState ?: return@Observer
            userState.success?.let {
                updateUserView(it)
            }
            userState.expiredError?.let {
                logout(it)
            }
            userState.notLoginError?.let {
                logout(it)
            }
        })

        return root
    }

    private fun updateUserView(loggedInUserView: LoggedInUserView) {
        loginName.text = loggedInUserView.displayName
        accountLayout.setOnClickListener {
            findNavController().navigate(R.id.action_navigation_mine_to_navigation_account)
        }
    }

    private fun logout(@StringRes message: Int) {
        loginName.text = getString(message)
        loginName.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.navigation_login))
    }
}