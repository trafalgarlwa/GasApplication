package com.example.gasapplication.ui.device

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.gasapplication.data.model.DeviceInfo

class DeviceItemAdapter(
    private val list: MutableList<DeviceInfo>,
    val onStateButtonClickListener: (DeviceItemView, Int, DeviceItemAdapter) -> Unit,
    val onRestartButtonClickListener: (DeviceItemView, Int, DeviceItemAdapter) -> Unit,
    val onOptionButtonClickListener: (Int, DeviceItemAdapter) -> Unit,
    val onItemClickListener: (Int) -> Unit
) : RecyclerView.Adapter<DeviceItemAdapter.VH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(DeviceItemView(parent.context))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val info = list[position]
        holder.root.apply {
            nameText.text = info.name
            setState(info.running)
            rootLayout.setOnClickListener { onItemClickListener(position) }
            stateButton.setOnClickListener {
                onStateButtonClickListener(this, position, this@DeviceItemAdapter)
            }
            restartButton.setOnClickListener {
                onRestartButtonClickListener(
                    this,
                    position,
                    this@DeviceItemAdapter
                )
            }
            optionButton.setOnClickListener {
                onOptionButtonClickListener(
                    position,
                    this@DeviceItemAdapter
                )
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class VH(view: View) : RecyclerView.ViewHolder(view) {
        val root = view as DeviceItemView
    }
}
