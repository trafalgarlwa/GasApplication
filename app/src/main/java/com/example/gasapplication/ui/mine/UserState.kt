package com.example.gasapplication.ui.mine

import com.example.gasapplication.ui.login.LoggedInUserView

data class UserState(
    val success: LoggedInUserView? = null,
    val notLoginError: Int? = null,
    val expiredError: Int? = null,
)