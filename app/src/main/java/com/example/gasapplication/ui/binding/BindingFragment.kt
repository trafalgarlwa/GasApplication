package com.example.gasapplication.ui.binding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.gasapplication.R
import dagger.hilt.android.AndroidEntryPoint

private const val ARG_LINK = "link"
private const val ARG_UUID = "uuid"

@AndroidEntryPoint
class BindingFragment : Fragment() {

    private val bindingViewModel by viewModels<BindingViewModel>()

    private var link: String? = null
    private var uuid: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            link = it.getString(ARG_LINK)
            uuid = it.getString(ARG_UUID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_binding, container, false)
        val textView = root.findViewById<TextView>(R.id.binding_uuid)
        val confirmButton = root.findViewById<Button>(R.id.confirm_button)
        val cancelButton = root.findViewById<Button>(R.id.cancel_button)
        val loading = root.findViewById<ProgressBar>(R.id.loading)

        textView.text = uuid

        confirmButton.setOnClickListener {
            loading.visibility = View.VISIBLE
            link?.let { bindingViewModel.binding(it) }
        }

        cancelButton.setOnClickListener {
            findNavController().navigateUp()
        }

        bindingViewModel.bindingState.observe(viewLifecycleOwner, Observer { state ->
            state ?: return@Observer
            loading.visibility = View.GONE
            state.success?.let {
                Toast.makeText(requireActivity(), it, Toast.LENGTH_SHORT).show()
                findNavController().navigateUp()
            }
            state.notFoundErr?.let {
                Toast.makeText(requireActivity(), it, Toast.LENGTH_SHORT).show()
            }
            state.networkErr?.let {
                Toast.makeText(requireActivity(), it, Toast.LENGTH_SHORT).show()
            }
        })


        return root
    }

    companion object {

        fun newBundle(link: String, uuid: String) = Bundle().apply {
            putString(ARG_LINK, link)
            putString(ARG_UUID, uuid)
        }
    }
}