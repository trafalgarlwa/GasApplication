package com.example.gasapplication.ui.dashboard

import android.app.Service
import android.content.Intent
import android.os.IBinder
import kotlin.concurrent.fixedRateTimer

private const val TAG = "NotificationService"

class NotificationService : Service() {

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        fixedRateTimer(period = 5000) {
            sendBroadcast(Intent(NOTIFICATION_ACTION))
        }
    }
}