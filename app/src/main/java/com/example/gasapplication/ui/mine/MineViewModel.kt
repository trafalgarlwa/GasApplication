package com.example.gasapplication.ui.mine

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.gasapplication.R
import com.example.gasapplication.api.AccountManager
import com.example.gasapplication.api.ApiService
import com.example.gasapplication.data.model.User
import com.example.gasapplication.ui.login.LoggedInUserView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MineViewModel @ViewModelInject constructor(
    private val apiService: ApiService
) : ViewModel() {
    val TAG = "MineViewModel"

    private val _userState = MutableLiveData<UserState>()
    val userState: LiveData<UserState> = _userState

    fun loadUser() {
        if (AccountManager.isLogin()) {
            _userState.value = UserState(success = LoggedInUserView(AccountManager.user!!.name))
            return
        }
        apiService.getUserInfo().enqueue(object : Callback<User?> {
            override fun onResponse(call: Call<User?>, response: Response<User?>) {
                when (response.code()) {
                    200 -> {
                        response.body()?.let {
                            AccountManager.user = it
                            _userState.value = UserState(success = LoggedInUserView(it.name))
                        }
                    }
                    else -> {
                        _userState.value = UserState(notLoginError = R.string.not_logged_in)
                    }
                }
            }

            override fun onFailure(call: Call<User?>, t: Throwable) {
                _userState.value = UserState(notLoginError = R.string.not_logged_in)
                Log.w(TAG, t)
            }
        })
    }
}