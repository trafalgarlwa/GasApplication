package com.example.gasapplication.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.gasapplication.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class ActionBottomDialogFragment : BottomSheetDialogFragment() {

    private var onRenameActionClickListener: View.OnClickListener? = null
    private var onShareActionClickListener: View.OnClickListener? = null
    private var onUnbindActionListener: View.OnClickListener? = null

    private lateinit var renameAction: TextView
    private lateinit var shareAction: TextView
    private lateinit var unbindAction: TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_options, container, false)
        renameAction = root.findViewById(R.id.rename_action)
        shareAction = root.findViewById(R.id.share_action)
        unbindAction = root.findViewById(R.id.unbind_action)
        val cancelAction = root.findViewById<TextView>(R.id.cancel_action)
        renameAction.setOnClickListener(this.onRenameActionClickListener)
        shareAction.setOnClickListener(this.onShareActionClickListener)
        unbindAction.setOnClickListener(this.onUnbindActionListener)
        cancelAction.setOnClickListener { dismiss() }
        return root
    }

    fun setOnRenameActionClickListener(l: View.OnClickListener) {
        this.onRenameActionClickListener = l
    }

    fun setOnShareActionListener(l: View.OnClickListener) {
        this.onShareActionClickListener = l
    }

    fun setOnUnbindActionListener(l: View.OnClickListener) {
        this.onUnbindActionListener = l
    }
}