package com.example.gasapplication.ui.login

import android.content.SharedPreferences
import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.gasapplication.R
import com.example.gasapplication.api.AccountManager
import com.example.gasapplication.api.ApiService
import com.example.gasapplication.data.model.User
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginViewModel @ViewModelInject constructor(
    private val apiService: ApiService,
    private val sp: SharedPreferences
) : ViewModel() {
    val TAG = "LoginViewModel"

    private val _loginForm = MutableLiveData<LoginFormState>()
    val loginFormState: LiveData<LoginFormState> = _loginForm

    private val _loginResult = MutableLiveData<LoginResult>()
    val loginResult: LiveData<LoginResult> = _loginResult

    fun checkLogin() {
        apiService.getUserInfo().enqueue(object : Callback<User?> {
            override fun onResponse(call: Call<User?>, response: Response<User?>) {
                when (response.code()) {
                    200 -> {
                        response.body()?.let {
                            AccountManager.user = it
                        }
                        _loginResult.value = LoginResult(success = 0)
                    }
                }
            }

            override fun onFailure(call: Call<User?>, t: Throwable) {
                Log.w(TAG, t)
            }
        })
    }


    fun login(username: String, password: String) {
        val call = apiService.login(username, password)
        call.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                when (response.code()) {
                    200, 302 -> {
                        val headers = response.headers().values("Set-Cookie")
                        if (headers.isNotEmpty()) {
                            val cookies = headers.toSet()
                            val editor = sp.edit()
                            editor.putStringSet("cookies", cookies)
                            editor.apply()
                        }
                        _loginResult.value = LoginResult(success = R.string.login_success)
                    }
                    else -> {
                        _loginResult.value = LoginResult(error = R.string.login_failed)
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                Log.w(TAG, t)
                _loginResult.value = LoginResult(error = R.string.network_exception)
            }
        })
    }

    fun loginDataChanged(username: String, password: String) {
        if (!isUserNameValid(username)) {
            _loginForm.value = LoginFormState(usernameError = R.string.invalid_username)
        } else if (!isPasswordValid(password)) {
            _loginForm.value = LoginFormState(passwordError = R.string.invalid_password)
        } else {
            _loginForm.value = LoginFormState(isDataValid = true)
        }
    }

    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return username.isNotEmpty()
    }

    // A placeholder password validation check
    private fun isPasswordValid(password: String): Boolean {
        return password.length > 5
    }
}