package com.example.gasapplication.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.example.gasapplication.R
import com.example.gasapplication.data.NotificationRepo
import java.text.SimpleDateFormat

class DashboardFragment : Fragment() {

    private lateinit var dashboardViewModel: DashboardViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
            ViewModelProvider(this).get(DashboardViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)
        root.findViewById<RecyclerView>(R.id.list_view).apply {
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            adapter =
                MessageItemAdapter(
                    NotificationRepo(requireContext()).getAllMessage().sortedWith { o1, o2 ->
                        sdf.parse(o2.createdAt).compareTo(sdf.parse(o1.createdAt))
                    }
                )
        }
        return root
    }

}