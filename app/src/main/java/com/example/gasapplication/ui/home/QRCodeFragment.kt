package com.example.gasapplication.ui.home

import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.example.gasapplication.R
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException
import javax.inject.Inject

private const val ARG_UUID = "uuid"

@AndroidEntryPoint
class QRCodeFragment : Fragment() {

    @Inject
    lateinit var client: OkHttpClient

    private var uuid: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            uuid = it.getString(ARG_UUID)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dialog_qrcode, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val url = "http://hmh.xming.club:3000/api/qrcode?text="
        val text = "http://hmh.xming.club:3000/api/devices/bind?uuid=$uuid"
        client.newCall(
            Request.Builder().url("$url$text").build()
        ).enqueue(object : okhttp3.Callback {
            override fun onFailure(call: okhttp3.Call, e: IOException) {
            }

            override fun onResponse(call: okhttp3.Call, response: okhttp3.Response) {
                response.body()?.let {
                    val bitmap = BitmapFactory.decodeStream(it.byteStream())
                    requireActivity().runOnUiThread {
                        view.findViewById<ImageView>(R.id.image).apply {
                            setImageBitmap(bitmap)
                        }
                    }
                }
            }
        })

    }

    companion object {

        @JvmStatic
        fun newBundle(uuid: String) =
            Bundle().apply {
                putString(ARG_UUID, uuid)
            }
    }
}