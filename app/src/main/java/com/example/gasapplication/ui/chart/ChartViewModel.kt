package com.example.gasapplication.ui.chart

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.gasapplication.api.ApiService
import com.example.gasapplication.data.model.GasMap
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.concurrent.fixedRateTimer

private const val TAG = "ChartViewModel"

class ChartViewModel @ViewModelInject constructor(
    private val apiService: ApiService
) : ViewModel() {

    val records = MutableLiveData<Result<GasMap>>()
    lateinit var timer: Timer

    fun loadRecords(id: Int) {
        timer = fixedRateTimer(period = 5000) {
            apiService.listRecords(id).enqueue(object : Callback<GasMap?> {
                override fun onResponse(call: Call<GasMap?>, response: Response<GasMap?>) {
                    when (response.code()) {
                        200 -> {
                            response.body()?.let {
                                records.value = Result.success(it)
                            }
                        }
                        else -> {
                        }
                    }
                }

                override fun onFailure(call: Call<GasMap?>, t: Throwable) {
                    records.value = Result.failure(t)
                }
            })
        }
    }

    fun exposed(id: Int) {
        apiService.exposedDevice(id).enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                when (response.code()) {
                    201 -> {
                    }
                    else -> {
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                Log.w(TAG, t)
            }
        })
    }

    override fun onCleared() {
        super.onCleared()
        timer.cancel()
        Log.d(TAG, "timer canceled")
    }
}