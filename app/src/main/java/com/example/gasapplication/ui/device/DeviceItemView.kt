package com.example.gasapplication.ui.device

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.Button
import android.widget.ImageButton
import android.widget.RelativeLayout
import android.widget.TextView
import com.example.gasapplication.R

class DeviceItemView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) :
    RelativeLayout(context, attrs, defStyleAttr) {

    var rootLayout: RelativeLayout
    var nameText: TextView
    var stateText: TextView
    var stateButton: Button
    var restartButton: Button
    var optionButton: ImageButton

    var running = false

    init {
        val root = LayoutInflater.from(context).inflate(R.layout.device_item, this, true)
        rootLayout = root.findViewById(R.id.root_layout)
        nameText = root.findViewById(R.id.name_text)
        stateText = root.findViewById(R.id.state_text)
        stateButton = root.findViewById(R.id.state_button)
        restartButton = root.findViewById(R.id.restart_button)
        optionButton = root.findViewById(R.id.option_button)
        setState(running)
    }

    fun setState(running: Boolean) {
        this.running = running
        stateText.setBackgroundResource(if (running) R.drawable.running_label else R.drawable.stopped_label)
        stateText.text =
            resources.getText(if (running) R.string.prompt_running else R.string.prompt_stopped)
        stateButton.text =
            resources.getText(if (running) R.string.prompt_shutdown else R.string.prompt_turn_on)
    }

    fun changeState() {
        setState(!running)
    }
}