package com.example.gasapplication.ui.home

import android.app.AlertDialog
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.viewModelScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.gasapplication.R
import com.example.gasapplication.api.ApiService
import com.example.gasapplication.data.model.DeviceInfo
import com.example.gasapplication.ui.binding.BindingFragment
import com.example.gasapplication.ui.chart.ChartFragment
import com.example.gasapplication.ui.dashboard.NOTIFICATION_ACTION
import com.example.gasapplication.ui.dashboard.NotificationReceiver
import com.example.gasapplication.ui.dashboard.NotificationService
import com.example.gasapplication.ui.device.DeviceItemAdapter
import com.google.zxing.integration.android.IntentIntegrator
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import okhttp3.*
import java.util.*
import javax.inject.Inject

private const val TAG = "HomeFragment"

@AndroidEntryPoint
class HomeFragment : Fragment() {

    @Inject
    lateinit var okHttpClient: OkHttpClient

    @Inject
    lateinit var apiService: ApiService

    private val homeViewModel by viewModels<HomeViewModel>()
    private lateinit var listView: RecyclerView
    private lateinit var loading: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().startService(Intent(context, NotificationService::class.java))
        requireContext().registerReceiver(
            NotificationReceiver(apiService),
            IntentFilter(NOTIFICATION_ACTION)
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        listView = root.findViewById(R.id.list_view)
        loading = root.findViewById(R.id.loading)
        root.findViewById<SwipeRefreshLayout>(R.id.swipe_layout).apply {
            setOnRefreshListener {
                homeViewModel.loadDevices()
                this.isRefreshing = false
            }
        }

        homeViewModel.loadDevices()
        homeViewModel.devices.observe(viewLifecycleOwner, devicesObserver())

        homeViewModel.loadingResult.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            loading.visibility = View.GONE
        })
        setHasOptionsMenu(true)
        return root
    }

    fun devicesObserver(): Observer<List<DeviceInfo>> {
        return Observer {
            it ?: return@Observer
            val list = it.toMutableList()
            listView.adapter = DeviceItemAdapter(list, { itemView, position, adapter ->
                // on state button clicked
                val device = list[position]
                if (device.running) {
                    AlertDialog.Builder(context)
                        .setTitle("关机")
                        .setMessage("确定要关机吗？")
                        .setPositiveButton(
                            "关机"
                        ) { _, _ ->
                            loading.visibility = View.VISIBLE
                            homeViewModel.stopDevice(device.id)
                            list[position] = device.copy(running = !device.running)
                            itemView.changeState()
                            adapter.notifyItemChanged(position)
                        }
                        .setNegativeButton("取消", null)
                        .show()
                } else {
                    loading.visibility = View.VISIBLE
                    homeViewModel.startDevice(device.id)
                    list[position] = device.copy(running = !device.running)
                    itemView.changeState()
                    adapter.notifyItemChanged(position)
                }
            }, { itemView, position, adapter ->
                // on restart button clicked
                if (list[position].running) {
                    itemView.changeState()
                }
                loading.visibility = View.VISIBLE
                homeViewModel.viewModelScope.launch {
                    delay(1500)
                    homeViewModel.startDevice(list[position].id)
                    itemView.changeState()
                }
            }, { position, adapter ->
                // on option button clicked
                val editText =
                    EditText(context).apply {
                        setText(list[position].name, TextView.BufferType.EDITABLE)
                    }
                ActionBottomDialogFragment().apply {
                    // rename action
                    setOnRenameActionClickListener {
                        dismiss()
                        AlertDialog.Builder(context)
                            .setTitle("重命名")
                            .setMessage("输入新名称")
                            .setView(editText)
                            .setPositiveButton(R.string.action_confirm) { _, _ ->
                                homeViewModel.renameDevice(
                                    list[position].id,
                                    editText.text.toString()
                                )
                                list[position] =
                                    list[position].copy(name = editText.text.toString())
                                adapter.notifyItemChanged(position)
                            }
                            .setNegativeButton(R.string.action_cancel) { _, _ -> }
                            .show()
                    }

                    // share action
                    setOnShareActionListener {
                        findNavController().navigate(
                            R.id.action_navigation_home_to_navigation_qrcode,
                            QRCodeFragment.newBundle(list[position].uuid)
                        )
                        dismiss()
                    }

                    // unbind action
                    setOnUnbindActionListener {
                        homeViewModel.unbindDevice(list[position].id)
                        list.removeAt(position)
                        adapter.notifyItemRemoved(position)
                        adapter.notifyItemRangeChanged(position, list.size - position)
                        dismiss()
                    }
                }.show(parentFragmentManager, "actionBottomDialogFragment")
            }, { position ->
                // on item clicked
                if (list[position].running) {
                    findNavController().navigate(
                        R.id.action_navigation_home_to_navigation_chart,
                        ChartFragment.newBundle(list[position])
                    )
                }
            })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        result?.contents?.let { it ->
            val link = it.trim()
            val regex =
                Regex("http://hmh.xming.club:3000/api/devices/bind\\?uuid=([0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12})")
            val matched = regex.matchEntire(link)
            matched?.let {
                val uuid = it.destructured.component1()
                findNavController().navigate(
                    R.id.action_navigation_home_to_navigation_binding,
                    BindingFragment.newBundle(link, uuid)
                )
            } ?: Toast.makeText(activity, "未知设备", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.scanner_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        IntentIntegrator.forSupportFragment(this).apply {
            setPrompt("请扫描设备二维码")
            setDesiredBarcodeFormats(IntentIntegrator.QR_CODE)
        }.initiateScan()
        return true
    }
}