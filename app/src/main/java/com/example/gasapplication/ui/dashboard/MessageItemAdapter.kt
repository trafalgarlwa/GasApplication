package com.example.gasapplication.ui.dashboard

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.gasapplication.R
import com.example.gasapplication.data.model.Message

class MessageItemAdapter(val list: List<Message>) : RecyclerView.Adapter<MessageItemAdapter.VH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(LayoutInflater.from(parent.context).inflate(R.layout.message_item, parent, false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        list[position].apply {
            holder.createdText.text = createdAt
            holder.upperText.text = upper
            holder.lowerText.text = lower
            holder.envText.text = env
            holder.levelText.text = if (type == "high") "高" else "中"
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var createdText = itemView.findViewById<TextView>(R.id.created_time)
        var levelText = itemView.findViewById<TextView>(R.id.level)
        var upperText = itemView.findViewById<TextView>(R.id.upper)
        var lowerText = itemView.findViewById<TextView>(R.id.lower)
        var envText = itemView.findViewById<TextView>(R.id.env)
    }
}