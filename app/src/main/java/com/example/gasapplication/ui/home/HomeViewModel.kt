package com.example.gasapplication.ui.home

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.gasapplication.api.ApiService
import com.example.gasapplication.data.model.DeviceInfo
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeViewModel @ViewModelInject constructor(
    private val apiService: ApiService
) : ViewModel() {
    val TAG = "HomeViewModel"

    val devices = MutableLiveData<List<DeviceInfo>>()

    val loadingResult = MutableLiveData<Int>()

    fun loadDevices() {
        apiService.listDevices().enqueue(object : Callback<List<DeviceInfo>?> {
            override fun onResponse(
                call: Call<List<DeviceInfo>?>,
                response: Response<List<DeviceInfo>?>
            ) {
                when (response.code()) {
                    200 -> {
                        response.body()?.let {
                            devices.value = it
                        }
                    }
                    else -> {
                    }
                }
            }

            override fun onFailure(call: Call<List<DeviceInfo>?>, t: Throwable) {
                Log.w(TAG, t)
            }
        })
    }

    fun startDevice(id: Int) {
        apiService.startDevice(id).enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                loadingResult.value = 0
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                Log.w(TAG, t)
            }
        })
    }

    fun stopDevice(id: Int) {
        apiService.stopDevice(id).enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                loadingResult.value = 0
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                Log.w(TAG, t)
            }
        })
    }

    fun renameDevice(id: Int, name: String) {
        apiService.renameDevice(id, name).enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                when (response.code()) {
                    202 -> {
                        loadingResult.value = 0
                    }
                    else -> {
                        loadingResult.value = 0
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                loadingResult.value = 0
                Log.w(TAG, t)
            }
        })
    }

    fun unbindDevice(id: Int) {
        apiService.unbindDevice(id).enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                loadingResult.value = 0
            }

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                Log.w(TAG, t)
            }
        })
    }

}