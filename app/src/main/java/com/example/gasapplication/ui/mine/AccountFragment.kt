package com.example.gasapplication.ui.mine

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.gasapplication.R
import com.example.gasapplication.api.AccountManager

class AccountFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_account, container, false)
        val nicknameText = root.findViewById<TextView>(R.id.nickname_text)
            .apply { text = AccountManager.user!!.name }
        root.findViewById<RelativeLayout>(R.id.nickname_item).apply {
            setOnClickListener {
                findNavController().navigate(
                    R.id.action_navigation_account_to_navigation_name_edit,
                    NameEditFragment.newBundle(nicknameText.text.toString())
                )
            }
        }
        return root
    }
}