package com.example.gasapplication.ui.dashboard

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.gasapplication.R
import com.example.gasapplication.api.AccountManager
import com.example.gasapplication.api.ApiService
import com.example.gasapplication.data.NotificationRepo
import com.example.gasapplication.data.model.Message
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

private const val TAG = "NotificationReceiver"
private const val CHANNEL_ID = "NotificationReceiver"

const val NOTIFICATION_ACTION = "SEND_NOTIFICATION"

class NotificationReceiver(val apiService: ApiService) : BroadcastReceiver() {
    private var notificationId = 0

    override fun onReceive(context: Context, intent: Intent) {
        notificationId++
        apiService.subscribe(AccountManager.user!!.id).enqueue(object : Callback<Message?> {
            override fun onResponse(call: Call<Message?>, response: Response<Message?>) {
                if (response.code() == 200) {
                    response.body()?.let { message ->
                        Log.d(TAG, message.toString())
                        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                        Calendar.getInstance().apply {
                            time = sdf.parse(message.createdAt)
                            add(Calendar.HOUR, 8)
                            message.createdAt = sdf.format(time)
                        }
                        NotificationRepo(context).insert(message)
                        Log.d(TAG, message.toString())
                        with(NotificationManagerCompat.from(context.applicationContext)) {
                            notify(
                                notificationId,
                                NotificationCompat.Builder(context.applicationContext, CHANNEL_ID)
                                    .setSmallIcon(R.mipmap.ic_launcher)
                                    .setContentTitle("有一条警告")
                                    .setContentText("${message.createdAt}, env=${message.env}, lower=${message.lower}, upper=${message.upper}")
                                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                                    .setCategory(NotificationCompat.CATEGORY_CALL)
                                    .setAutoCancel(true).build()
                            )
                        }
                    }
                }
            }

            override fun onFailure(call: Call<Message?>, t: Throwable) {
                Log.w(TAG, t)
            }
        })
    }
}