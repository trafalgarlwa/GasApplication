package com.example.gasapplication.ui.chart

import android.graphics.Color
import android.os.Bundle
import android.view.*
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.gasapplication.R
import com.example.gasapplication.data.model.DeviceInfo
import com.example.gasapplication.data.model.GasMap
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import dagger.hilt.android.AndroidEntryPoint

private const val TAG = "ChartFragment"

private const val ARG_DEVICE = "device"

@AndroidEntryPoint
class ChartFragment : Fragment() {

    lateinit var chart: LineChart

    companion object {
        fun newBundle(device: DeviceInfo) = Bundle().apply {
            putSerializable(ARG_DEVICE, device)
        }
    }

    private val viewModel by viewModels<ChartViewModel>()

    private lateinit var device: DeviceInfo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            device = it.get(ARG_DEVICE) as DeviceInfo
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_chart, container, false)
        root.findViewById<TextView>(R.id.name_text).apply { text = device.name }
        chart = root.findViewById<LineChart>(R.id.chart).apply {

            description = Description().apply { text = getString(R.string.prompt_data_update) }
            setDrawGridBackground(false)
            axisRight.setDrawLabels(false)
            axisRight.setDrawGridLines(false)
            axisLeft.setDrawGridLines(false)
            xAxis.apply {
                isEnabled = false
                setDrawLabels(false)
                setDrawGridLines(false)
            }
        }

        root.findViewById<Button>(R.id.exposed_button).apply {
            setOnClickListener {
                viewModel.exposed(device.id)
            }
        }

        viewModel.loadRecords(device.id)
        viewModel.records.observe(viewLifecycleOwner, Observer { result ->
            result ?: return@Observer
            result.onSuccess { mp ->
                chart.data = LineData(
                    LineDataSet(mp.mpToList("upper_limit"), "爆炸上限")
                        .apply { color = Color.RED },
                    LineDataSet(mp.mpToList("lower_limit"), "爆炸下限")
                        .apply { color = Color.rgb(189, 144, 40) },
                    LineDataSet(mp.mpToList("env"), "天然气含量").apply { color = Color.GREEN },
                )
                fun Double.format(digits: Int) = "%.${digits}f%%".format(this)
                root.findViewById<TextView>(R.id.upper_text)
                    .apply {
                        text =
                            if (!mp["upper_limit"].isNullOrEmpty()) mp["upper_limit"]?.last()?.value?.times(
                                100
                            )?.format(2) else "--"
                    }
                root.findViewById<TextView>(R.id.lower_text)
                    .apply {
                        text =
                            if (!mp["lower_limit"].isNullOrEmpty()) mp["lower_limit"]?.last()?.value?.times(
                                100
                            )?.format(2) else "--"
                    }
                root.findViewById<TextView>(R.id.env_text)
                    .apply {
                        text =
                            if (!mp["env"].isNullOrEmpty()) mp["env"]?.last()?.value?.times(
                                100
                            )?.format(2) else "--"
                    }
                chart.notifyDataSetChanged()
                chart.invalidate()
            }
        })
        return root
    }

    fun GasMap.mpToList(key: String): List<Entry> {
        val mutableList = mutableListOf<Entry>()
        this[key]?.forEachIndexed { index, data ->
            mutableList.add(
                Entry(
                    index.toFloat(),
                    data.value.toFloat()
                )
            )
        }
        return mutableList
    }
}