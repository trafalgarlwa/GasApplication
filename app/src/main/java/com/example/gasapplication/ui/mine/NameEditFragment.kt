package com.example.gasapplication.ui.mine

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.EditText
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.gasapplication.R
import com.example.gasapplication.api.AccountManager
import com.example.gasapplication.api.ApiService
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject


private const val TAG = "NameEditFragment"
private const val ARG_NAME = "name"

@AndroidEntryPoint
class NameEditFragment : Fragment() {

    @Inject
    lateinit var apiService: ApiService

    private var name: String? = null

    private lateinit var nameEdit: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            name = it.getString(ARG_NAME)
        }
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (requireActivity() as AppCompatActivity).supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_baseline_close_24)
        val root = inflater.inflate(R.layout.fragment_name_edit, container, false)
        nameEdit = root.findViewById<EditText>(R.id.name_edit).apply {
            setText(name)
        }
        setHasOptionsMenu(true)
        return root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.done_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (nameEdit.text.isEmpty()) {
            nameEdit.error = "昵称不能为空"
            return false
        }
        apiService.updateUserInfo(nameEdit.text.toString())
            .enqueue(object : Callback<ResponseBody?> {
                override fun onResponse(
                    call: Call<ResponseBody?>,
                    response: Response<ResponseBody?>
                ) {
                    AccountManager.user?.apply {
                        name = nameEdit.text.toString()
                        findNavController().navigateUp()
                    }
                }

                override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                    Log.w(TAG, t)
                }
            })
        return true
    }

    companion object {

        fun newBundle(name: String) = Bundle().apply { putString(ARG_NAME, name) }
    }
}