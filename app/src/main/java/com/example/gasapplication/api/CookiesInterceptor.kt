package com.example.gasapplication.api

import android.content.SharedPreferences
import okhttp3.Interceptor
import okhttp3.Response

class ReceivedCookiesInterceptor(private val sp: SharedPreferences) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())
        val headers = response.headers("Set-Cookie")
        if (headers.isNotEmpty()) {
            val cookies = headers.toSet()
            val editor = sp.edit()
            editor.putStringSet("cookies", cookies)
            editor.apply()
        }
        return response
    }
}

class AddCookiesInterceptor(private val sp: SharedPreferences) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val builder = chain.request().newBuilder()
        val cookies = sp.getStringSet("cookies", setOf())
        cookies?.forEach { builder.addHeader("Cookie", it) }
        return chain.proceed(builder.build())
    }
}