package com.example.gasapplication.api

import com.example.gasapplication.data.model.User

class AccountManager {
    companion object {
        var user: User? = null

        fun isLogin(): Boolean {
            return user != null
        }
    }
}