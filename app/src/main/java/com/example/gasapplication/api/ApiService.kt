package com.example.gasapplication.api

import com.example.gasapplication.data.model.DeviceInfo
import com.example.gasapplication.data.model.GasMap
import com.example.gasapplication.data.model.Message
import com.example.gasapplication.data.model.User
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    @POST("auth")
    @FormUrlEncoded
    fun login(
        @Field("username") username: String,
        @Field("password") password: String
    ): Call<ResponseBody>

    @GET("user")
    fun getUserInfo(): Call<User>

    @PATCH("user")
    @FormUrlEncoded
    fun updateUserInfo(@Field("name") name: String): Call<ResponseBody>

    @GET("device/{id}/record")
    fun listRecords(@Path("id") id: Int): Call<GasMap>

    @GET
    fun bindingDevice(@Url url: String): Call<ResponseBody>

    @GET("device")
    fun listDevices(): Call<List<DeviceInfo>>

    @POST("device/{id}/task")
    fun startDevice(@Path("id") id: Int): Call<ResponseBody>

    @DELETE("device/{id}/task")
    fun stopDevice(@Path("id") id: Int): Call<ResponseBody>

    @PUT("device/{id}")
    @FormUrlEncoded
    fun renameDevice(@Path("id") id: Int, @Field("name") name: String): Call<ResponseBody>

    @DELETE("device/{id}/bind")
    fun unbindDevice(@Path("id") id: Int): Call<ResponseBody>

    @PUT("device/{id}/task")
    fun exposedDevice(@Path("id") id: Int): Call<ResponseBody>

    @GET("subscribe/{id}")
    fun subscribe(@Path("id") id: Int): Call<Message>

    companion object {
        const val SERVER_URL = "http://hmh.xming.club:3000/api/"
    }
}