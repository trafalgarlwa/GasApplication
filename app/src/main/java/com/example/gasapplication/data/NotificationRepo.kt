package com.example.gasapplication.data

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.gasapplication.data.model.Message

class NotificationRepo(
    context: Context,
    name: String? = "gas_db",
    factory: SQLiteDatabase.CursorFactory? = null,
    version: Int = 1
) : SQLiteOpenHelper(context, name, factory, version) {

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL("CREATE TABLE message(type TEXT, upper TEXT, lower TEXT, env TEXT, created_at TEXT)")
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
    }

    fun insert(message: Message) {
        writableDatabase.insert("message", null, ContentValues().apply {
            put("type", message.type)
            put("upper", message.upper)
            put("lower", message.lower)
            put("env", message.env)
            put("created_at", message.createdAt)
        })
    }

    fun getAllMessage(): List<Message> {
        val list = mutableListOf<Message>()
        readableDatabase.query(
            false,
            "message",
            arrayOf("type", "upper", "lower", "env", "created_at"),
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        ).apply {
            while (moveToNext()) {
                list.add(Message().apply {
                    type = getString(getColumnIndex("type"))
                    upper = getString(getColumnIndex("upper"))
                    lower = getString(getColumnIndex("lower"))
                    env = getString(getColumnIndex("env"))
                    createdAt = getString(getColumnIndex("created_at"))
                })
            }
            close()
        }
        return list
    }
}