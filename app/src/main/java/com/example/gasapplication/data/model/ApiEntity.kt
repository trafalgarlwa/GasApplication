package com.example.gasapplication.data.model

import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable
import java.sql.Timestamp


data class DeviceInfo(
    @JsonProperty("id")
    val id: Int = 0,
    @JsonProperty("name")
    val name: String = "",
    @JsonProperty("uuid")
    val uuid: String = "",
    @JsonProperty("running")
    val running: Boolean = false
) : Serializable

data class User(
    @JsonProperty("id")
    var id: Int = 0,
    @JsonProperty("name")
    var name: String = "",
    @JsonProperty("created_at")
    val createdAt: Timestamp
) : Serializable

data class Message(
    @JsonProperty("type")
    var type: String = "high",
    @JsonProperty("upper")
    var upper: String = "",
    @JsonProperty("lower")
    var lower: String = "",
    @JsonProperty("env")
    var env: String = "",
    @JsonProperty("created_at")
    var createdAt: String = ""
) : Serializable