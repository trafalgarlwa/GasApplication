package com.example.gasapplication.data.model

import com.fasterxml.jackson.annotation.JsonProperty

class GasMap : HashMap<String, List<Data>>()

data class Data(
    @JsonProperty("timestamp") val timestamp: Long,
    @JsonProperty("value") val value: Double
)