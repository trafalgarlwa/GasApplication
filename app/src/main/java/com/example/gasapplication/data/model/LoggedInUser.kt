package com.example.gasapplication.data.model

import com.fasterxml.jackson.annotation.JsonProperty

data class LoggedInUser(
    @JsonProperty("sub")
    val sub: String = "",
    @JsonProperty("email_verified")
    val emailVerified: String = "",
    @JsonProperty("name")
    val name: String = "",
    @JsonProperty("preferred_username")
    val preferredUsername: String = "",
    @JsonProperty("family_name")
    val familyName: String = "",
    @JsonProperty("email")
    val email: String = ""
)
