package com.example.gasapplication.data.model

import com.fasterxml.jackson.annotation.JsonProperty

data class Token(
    @JsonProperty("access_token")
    val accessToken: String = "",
    @JsonProperty("expires_in")
    val expiresIn: Int = 0,
    @JsonProperty("refresh_expires_in")
    val refreshExpiresIn: Int = 0,
    @JsonProperty("refresh_token")
    val refreshToken: String = "",
    @JsonProperty("token_type")
    val tokenType: String = "",
    @JsonProperty("not-before-policy")
    val notBeforePolicy: Int = 0,
    @JsonProperty("session_state")
    val sessionState: String = "",
    @JsonProperty("scope")
    val scope: String = ""
)
