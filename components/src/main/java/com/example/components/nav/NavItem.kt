package com.example.components.nav

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.example.components.R

class NavItem(context: Context?, attrs: AttributeSet?) : RelativeLayout(context, attrs) {
    private val _root: RelativeLayout
    private val _icon: ImageView
    private val _text: TextView
    private val _underline: View

    init {
        val root = LayoutInflater.from(context).inflate(R.layout.nav_item, this)
        _root = root.findViewById(R.id.root_layout)
        _icon = root.findViewById(R.id.left_icon)
        _text = root.findViewById(R.id.left_text)
        _underline = root.findViewById(R.id.underline)
        context?.obtainStyledAttributes(attrs, R.styleable.NavItem)?.run {
            _icon.setImageDrawable(getDrawable(R.styleable.NavItem_leftIcon))
            _text.text = getString(R.styleable.NavItem_leftText)
            _underline.visibility = if (getBoolean(
                    R.styleable.NavItem_isUnderlineShown,
                    false
                )
            ) View.VISIBLE else View.GONE
            recycle()
        }


    }

    override fun setOnClickListener(l: OnClickListener?) {
        _root.setOnClickListener(l)
    }
}